// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "My32Project_gitHUD.generated.h"

UCLASS()
class AMy32Project_gitHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMy32Project_gitHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

