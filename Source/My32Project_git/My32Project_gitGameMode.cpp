// Copyright Epic Games, Inc. All Rights Reserved.

#include "My32Project_gitGameMode.h"
#include "My32Project_gitHUD.h"
#include "My32Project_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMy32Project_gitGameMode::AMy32Project_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMy32Project_gitHUD::StaticClass();
}
