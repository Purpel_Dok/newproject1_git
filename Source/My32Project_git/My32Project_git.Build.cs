// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class My32Project_git : ModuleRules
{
	public My32Project_git(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
