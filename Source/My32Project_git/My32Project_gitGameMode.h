// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "My32Project_gitGameMode.generated.h"

UCLASS(minimalapi)
class AMy32Project_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMy32Project_gitGameMode();
};



